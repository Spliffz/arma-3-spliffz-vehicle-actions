/*
	Vehicle Seat Picker
	Author: Spliffz <theSpliffz@gmail.com>
	2013
*/	

	
//spliffz_action_GB = {
	while {true} do {
		_cars = nearestObjects [player, ["Car"], 10];
		if(((player distance (_cars select 0)) > 5) || (player in (_cars select 0))) then {
			if (!isNil "spliffz_CAR_action0") then {
				player removeAction spliffz_CAR_action0;
				spliffz_CAR_action0 = nil;
			};
			if (!isNil "spliffz_CAR_action1") then {
				player removeAction spliffz_CAR_action1;
				spliffz_CAR_action1 = nil;
			};
			if (!isNil "spliffz_CAR_action2") then {
				player removeAction spliffz_CAR_action2;
				spliffz_CAR_action2 = nil;
			};
			if (!isNil "spliffz_CAR_action3") then {
				player removeAction spliffz_CAR_action3;
				spliffz_CAR_action3 = nil;
			};
			if (!isNil "spliffz_CAR_action4") then {
				player removeAction spliffz_CAR_action4;
				spliffz_CAR_action4 = nil;
			};
			if (!isNil "spliffz_CAR_action5") then {
				player removeAction spliffz_CAR_action5;
				spliffz_CAR_action5 = nil;
			};
			if (!isNil "spliffz_CAR_action6") then {
				player removeAction spliffz_CAR_action6;
				spliffz_CAR_action6 = nil;
			};
			if (!isNil "spliffz_CAR_action7") then {
				player removeAction spliffz_CAR_action7;
				spliffz_CAR_action7 = nil;
			};
			if (!isNil "spliffz_CAR_action8") then {
				player removeAction spliffz_CAR_action8;
				spliffz_CAR_action8 = nil;
			};
			if (!isNil "spliffz_CAR_action9") then {
				player removeAction spliffz_CAR_action9;
				spliffz_CAR_action9 = nil;
			};
			if (!isNil "spliffz_CAR_action10") then {
				player removeAction spliffz_CAR_action10;
				spliffz_CAR_action10 = nil;
			};
			if (!isNil "spliffz_CAR_action11") then {
				player removeAction spliffz_CAR_action11;
				spliffz_CAR_action11 = nil;
			};
		};
		
		_helis = nearestObjects [player, ["Helicopter"], 10];
		if(((player distance (_helis select 0)) > 5) || (player in (_helis select 0))) then {
			if (!isNil "spliffz_LB_action0") then {
				player removeAction spliffz_LB_action0;
				spliffz_LB_action0 = nil;
			};
			if (!isNil "spliffz_LB_action1") then {
				player removeAction spliffz_LB_action1;
				spliffz_LB_action1 = nil;
			};
			if (!isNil "spliffz_LB_action2") then {
				player removeAction spliffz_LB_action2;
				spliffz_LB_action2 = nil;
			};
			if (!isNil "spliffz_LB_action3") then {
				player removeAction spliffz_LB_action3;
				spliffz_LB_action3 = nil;
			};
			if (!isNil "spliffz_LB_action4") then {
				player removeAction spliffz_LB_action4;
				spliffz_LB_action4 = nil;
			};
			if (!isNil "spliffz_LB_action5") then {
				player removeAction spliffz_LB_action5;
				spliffz_LB_action5 = nil;
			};
		};

		sleep 0.01;
	};
//};

