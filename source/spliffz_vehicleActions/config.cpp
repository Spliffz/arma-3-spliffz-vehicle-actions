class CfgPatches 
{
  	class Spliffz_VehicleActions 
  	{
    	units[] = {};
    	weapons[] = {};
    	requiredVersion = 1.04;
    	requiredAddons[]= {};
    	version = 1.1;
		versionDesc = "Spliffz Vehicle Actions";
    	author[] = {"Spliffz"};
  	};
};


class Extended_PostInit_EventHandlers 
{
	Spliffz_VehicleActions_Init = "if(!isDedicated) then { execVM '\spliffz_vehicleActions\spliffz_PA_init.sqf'};"; 
};


// EOF