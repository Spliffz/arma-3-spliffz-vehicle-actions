Spliffz Vehicle Actions 1.1a
Author: Spliffz <theSpliffz@gmail.com>
2013

Player actions:
    - Get in vehicles (pick seat)
    

TODO:
- fix stryder and ifrit: won't display seat options!

[changelog]
v1.1a
- commented the annoying hint (whoops sorry bout that!)
v1.1
- fixed animation for getting in :), Thnx AlfaWar
- Now works with mods!
v1.0
- initial public release


Works with (tested!):
- Pomigit's PMC 
- BWMod


[DISCLAIMER]
I (Spliffz) take no responsibility for anything that might happen with your game, computer, life or anything else when using this (my) mod(s).
By acknowledging this, you are now allowed to use this mod and to modify the code, on the condition that you share your changes/the code with me/the community, so that we can all benefit from it.
And if you decide to use this script or parts from it in other scripts/mods or addons then be so kind to put my name in there too.



// EOF